const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: {index: './src/index.js', people: './src/people.js'},
  output: {
    path: path.resolve('dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
      }
    ],
  },
  
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      filename: 'people.html',
      chunks: ['people']
    })
  ],
}