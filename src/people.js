import 'babel-polyfill';
import Mustache from 'mustache';

const fetchPeopleAndVehicles = async id => {
  const people = await (await fetch(`https://swapi.dev/api/people/${id}/`)).json();
  const vehicles = await Promise.all(people.vehicles.map(async vehicleUrl =>
    (await fetch(vehicleUrl)).json()
  ));
  return {
    people,
    vehicles,
  }
};

const pageTemplate = `
  <div>
    <b>Name:</b> {{people.name}}
    <ul>
      <li><b>height:</b> {{people.height}}</li>
      <li><b>mass:</b> {{people.mass}}</li>
      <li><b>hair color:</b> {{people.hair_color}}</li>
      <li><b>skin color:</b> {{people.skin_color}}</li>
      <li><b>eye color:</b> {{people.eye_color}}</li>
      <li><b>birth year:</b> {{people.birth_year}}</li>
      <li><b>gender:</b> {{people.gender}}</li>
    </ul>
    <b>Vehicles:</b>
    <ul>
      {{#vehicles}}
        <li>
          <b>{{name}}</b>
          <ul>
            <li><b>Crew:</b> {{crew}}</li>
            <li><b>Max atmosphering speed:</b> {{max_atmosphering_speed}}</li>
            <li><b>Cargo capacity:</b> {{cargo_capacity}}</li>
          </ul>
        </li>
      {{/vehicles}}
    </ul>
  </div>
`;

(async () => {
  const searchParams = new URLSearchParams(window.location.search);
  const peopleId = searchParams.get('id');
  document.body.textContent = 'loading...';
  const {people, vehicles} = await fetchPeopleAndVehicles(peopleId);
  document.body.textContent = '';
  const parser = new DOMParser();
  document.body.appendChild(
    parser.parseFromString(Mustache.render(pageTemplate, {people, vehicles}), "text/html").body.children[0]
  );
})();