import 'babel-polyfill';
import Mustache from 'mustache';

const fetchPeopleData = async () => {
  let page = 1;
  let allPeoplesFetched = false;
  const peoples = [];
  while (!allPeoplesFetched) {
    const response = await (await fetch(`https://swapi.dev/api/people/?page=${page}`)).json();
    peoples.push(...response.results);
    allPeoplesFetched = response.next === null;
    page++;
  }
  return peoples;
};

const pageTemplate = `
  <ul>
    {{#peoples}}
      <li>
        <a href="/people?id={{id}}">{{name}}</a>
        <b>height:</b> {{height}},
        <b>mass:</b> {{mass}},
        <b>hair color:</b> {{hair_color}},
        <b>skin color:</b> {{skin_color}},
        <b>eye color:</b> {{eye_color}},
        <b>birth year:</b> {{birth_year}},
        <b>gender:</b> {{gender}}
      </li>
    {{/peoples}}
  </ul>
`;

(async () => {
  document.body.textContent = 'loading...';
  const peoples = (await fetchPeopleData()).map(people => ({
    ...people,
    id: people.url.split('/').slice(-2)[0],
  }));
  document.body.textContent = '';
  const parser = new DOMParser();
  document.body.appendChild(
    parser.parseFromString(Mustache.render(pageTemplate, {peoples}), "text/html").body.children[0]
  );
})();